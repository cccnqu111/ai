# Layer

* [Backpropagation for a Linear Layer](http://cs231n.stanford.edu/handouts/linear-backprop.pdf)
    * Y = WX, 
    * gL/gX = gL/gY * gY/gX = gL/gY * W^T (算式 24)
    * gL/gW = gL/gY * gY/gW = (X^T * gL/gY)^T (算式 25)

## 來源

* https://github.com/maciejbalawejder/MLalgorithms-collection/blob/main/Neural%20Network/Image_Classifier.ipynb

資料 mnist_test.csv, mnist_train.csv 需自以下網址下載

* https://www.kaggle.com/scaomath/simple-mnist-numpy-from-scratch

```
$ python mnist.py
start()
train()
epoch=0
epoch=1
epoch=2
epoch=3
epoch=4
plot()
Accuracy: 88.29 %
```

## 參考

* https://mlfromscratch.com/neural-network-tutorial/#/
    * https://github.com/casperbh96/Neural-Network-From-Scratch/


