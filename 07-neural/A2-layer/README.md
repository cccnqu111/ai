# Layer

* [Julian Eisenschlos: Neural Networks in 100 lines of pure Python](https://eisenjulian.github.io/deep-learning-in-100-lines/)


In NumPy, the basic type is a multidimensional array. Array assignments in NumPy are usually stored as n-dimensional arrays with the minimum type required to hold the objects in sequence, unless you specify the number of dimensions and type. NumPy performs operations element-by-element, so multiplying 2D arrays with * is not a matrix multiplication – it’s an element-by-element multiplication. (The @ operator, available since Python 3.5, can be used for conventional matrix multiplication.)