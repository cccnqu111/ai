# ChatGPT 給的，但這不是一個 compiler
class MiniCompiler:
    def __init__(self):
        self.indent_level = 0

    def compile(self, code):
        # Split the code into individual lines
        lines = code.split("\n")

        # Initialize the compiled code as an empty string
        compiled_code = ""

        # Iterate over each line of the code
        for line in lines:
            # Increment the indent level if the line ends with a colon
            if line.endswith(":"):
                self.indent_level += 1

            # Decrement the indent level if the line is "end"
            if line == "end":
                self.indent_level -= 1

            # Add the appropriate number of indentation spaces before the line
            compiled_line = " " * 4 * self.indent_level + line

            # Add the compiled line to the compiled code
            compiled_code += compiled_line + "\n"

        return compiled_code

# Create an instance of the MiniCompiler
compiler = MiniCompiler()

# Read the code from a file
with open("code.mini") as f:
    code = f.read()

# Compile the code
compiled_code = compiler.compile(code)

# Write the compiled code to a file
with open("code.py", "w") as f:
    f.write(compiled_code)
