# 教科書

1. 自編教材
2. [Dive into Deep Learning](https://d2l.ai/)
    * 10. Attention Mechanisms
    * 14. Natural Language Processing: Pretraining
    * 15. Natural Language Processing: Applications
    * 18.4.5. The Backpropagation Algorithm
    * 18.7. Maximum Likelihood
3. 动手学深度学习 Dive into Deep Learning 的中文版
    * 每一小节都是可以运行的 Jupyter 记事本
    * 14.8. 来自Transformers的双向编码器表示（BERT）
    * 14.9. 用于预训练BERT的数据集
    * 14.10. 预训练BERT